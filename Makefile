# Makefile for testing CppUTest install.
#
# After installing CppUTest, issuing
# $ make run
# should compile and run, producing the message:
# testRunner.cpp:31: error: Failure in TEST(DummyGroup, OnlyTest)
#	Fail me!
#
# .
# Errors (1 failures, 1 tests, 1 ran, 1 checks, 0 ignored, 0 filtered out, 0 ms)
#
# Your CppUTest system is now ready to make serious testing!


# Adapt the following line to your own environment
#CPPUTEST_HOME = /trabalho/Aulas/PSE/Software/CppUTest/cpputest-3.6/
CPPUTEST_HOME =/home/pf/Downloads/cpputest-3.8

# Next step is "historical information", but it is kept in this Makefile
# in case similar problems arise in the future. It concerns an issue that
# occurred in Red Hat based systems (Fedora, CentOS) to
# solve "fatal error: bits/c++config.h: No such file or directory"
#
# The solution in this example applies to Fedora 20 Heisenbug. Other
# distros may place c++config.h in a different path.
# The problem was solved by adding the following to CXXFLAGS
# CXXFLAGS += -I/usr/include/c++/4.8.3/i686-redhat-linux

# You shouldn't need to change none of the following.
CXX = g++
CXXFLAGS += -I$(CPPUTEST_HOME)/include

CPPUTEST_LIBS = ${CPPUTEST_HOME}/lib
# In some occasions, the definition of CPPUTEST_LIBS was required to be:
# CPPUTEST_LIBS = ${CPPUTEST_HOME}/cpputest_build/lib

dummytest: *.cpp
	${CXX} ${CXXFLAGS} *.cpp -L$(CPPUTEST_LIBS) -lCppUTest -o dummytest

clean:
	rm -f dummytest

run: dummytest
	- ./dummytest
